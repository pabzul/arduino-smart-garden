# Reading Soil Moisture Sensor in Serial

<figure><img src="../../.gitbook/assets/Sensor humedad plantas (1).jpg" alt=""><figcaption></figcaption></figure>

## Soil Moisture Sensor:

The soil moisture sensor is a device used to measure the moisture content in the soil. It helps monitor the water level in the soil, which is crucial for efficient plant growth.&#x20;

The sensor consists of two conductive probes that are inserted into the soil. As the soil's moisture level changes, the conductivity between the probes also changes. This change in conductivity is used to determine the moisture content in the soil.





### Pinout and Connections:

Look for the A0 connections on the Sensor Shield as marked on the following picture.

| Sensor: | Sensor Shield: |
| ------- | -------------- |
| VCC     | V              |
| GND     | G              |
| A0      | S              |



<figure><img src="../../.gitbook/assets/soil Moisture pinnout A0.jpg" alt=""><figcaption></figcaption></figure>

* Connect the VCC pin of the soil moisture sensor to the V pin on the Shield.
* Connect the GND pin of the soil moisture sensor to the G pin on the Shield.
* Connect the A0 pin of the soil moisture sensor to the S input pin on the Shield.

<figure><img src="../../.gitbook/assets/2017-04-15_3984-1536x1536.jpg" alt=""><figcaption><p>A0 Connections</p></figcaption></figure>



### Code:

Open a new file on the Arduino IDE, copy this code and paste it in the new file.

```arduino
// Soil Moisture Sensor Pin
int soilMoisturePin = A0;

void setup() {
  // Initialize serial communication
  Serial.begin(9600);
}

void loop() {
  // Read soil moisture value
  int soilMoisture = analogRead(soilMoisturePin);

  // Print soil moisture value to serial monitor
  Serial.print("Soil Moisture: ");
  Serial.println(soilMoisture);

  // Delay before the next reading
  delay(1000);
}

```

#### Code Explanation:

* The code starts by declaring a variable `soilMoisturePin` to store the analog pin A0, which is connected to the soil moisture sensor.
* In the `setup()` function, the serial communication is initialized with a baud rate of 9600 using `Serial.begin()`.
* Within the `loop()` function, the soil moisture value is read using `analogRead()` from the `soilMoisturePin` and stored in the `soilMoisture` variable.
* The soil moisture value is then printed to the serial monitor using `Serial.print()` to display a label "Soil Moisture: " and `Serial.println()` to print the actual value.
* A delay of 1 second is added using `delay()` to wait before the next reading, preventing rapid data updates and making it easier to read the values.







## Calibrating the Soil Moisture Sensor

By calibrating the sensor, we can establish the minimum and maximum values of moisture levels in the soil, allowing us to set appropriate thresholds for watering our plants.



### Code:

```arduino
// Soil Moisture Sensor Pin
int soilMoisturePin = A0;

// Calibration Variables
int wet = 0;
int dry = 1023;

// Mapping Variables
int dryMap = 0;
int wetMap = 100;


void setup() {
  // Initialize serial communication
  Serial.begin(9600);
}

void loop() {
  // Read sensor value
  int sensorValue = analogRead(soilMoisturePin);

  // Map the sensor value to the mapped range
  int mappedValue = map(sensorValue, dry, wet, dryMap, wetMap);

  // Display sensor value and mapped value
  Serial.print("Sensor Value: ");
  Serial.print(sensorValue);
  Serial.print("\tMapped Value: ");
  Serial.print(mappedValue);
  Serial.println( "%");

  // Delay for 1 second
  delay(1000);

}
```

#### Code Explanation:

* It reads the sensor value from the soil moisture sensor and maps it to the desired range using the `map()` function.&#x20;
* The sensor value, along with the corresponding mapped value in percentage, is then printed on the serial monitor.
* The `wet` and `dry` variables represent the calibration values obtained during wet and dry calibration, respectively.&#x20;
* These values define the range of the sensor readings. The `dryMap` and `wetMap` variables represent the desired mapped range, which is from 0% to 100%.
* The code runs in the `loop()` function, continuously reading the sensor value, mapping it, and printing the results on the serial monitor.
* The delay of 1 second ensures that the values are displayed at a reasonable interval.

