---
cover: >-
  https://images.unsplash.com/photo-1515879218367-8466d910aaa4?crop=entropy&cs=srgb&fm=jpg&ixid=M3wxOTcwMjR8MHwxfHNlYXJjaHw4fHxjb2RlfGVufDB8fHx8MTY5NjkzNjIzMXww&ixlib=rb-4.0.3&q=85
coverY: 0
---

# Codes

Welcome to the code index page for the Smart Garden Automation project with Arduino! Here, you will find a collection of the codes used throughout the project, each focusing on a specific aspect of the automation system.

1. [**Reading Soil Moisture Sensor in Serial:**](reading-soil-moisture-sensor-in-serial/) Learn how to read data from the soil moisture sensor and display it in the serial monitor. This code will help you understand the moisture level of your garden soil.
2. [**Connecting and Displaying Messages on LCD:** ](connecting-and-displaying-messages-on-lcd.md)Discover how to connect and configure an LCD display to your Arduino board. This code will enable you to display custom messages and information on the LCD screen.
3. [**Reading Temperature and Humidity with DHT Sensor:**](reading-temperature-and-humidity-with-dht-sensor.md) Explore how to interface the DHT11 sensor with your Arduino to measure temperature and humidity. This code allows you to retrieve accurate environmental data.
4. [**Displaying DHT Sensor Values on LCD**:](displaying-dht-sensor-values-on-lcd.md) Combine the DHT sensor and LCD display to showcase real-time temperature and humidity readings on the LCD screen. This code integrates the two components seamlessly.
5. [**Controlling Water Pump with Soil Moisture Threshold:**](controlling-water-pump-with-soil-moisture-threshold.md) Learn how to control a water pump based on the soil moisture level. This code ensures that the pump turns on or off according to the predefined moisture threshold.
6. [**Smart Garden Automation with Arduino (full integration):**](smart-garden-automation-with-arduino.md) Achieve the ultimate automation system by integrating all the components. This comprehensive code combines soil moisture sensing, temperature and humidity monitoring, LCD display feedback, and water pump control.

By navigating through this index, you can access and explore each code individually to better understand how the different aspects of the Smart Garden Automation project come together. Enjoy coding and optimizing your automated garden experience!
