# Reading Temperature and Humidity with DHT Sensor

<figure><img src="../.gitbook/assets/1824_1c07a19c-cac8-4643-82a7-8386e17f3b880.jpg" alt=""><figcaption></figcaption></figure>

## DHT11 Sensor:

DHT sensors are a type of digital temperature and humidity sensor that combine both sensing elements into a single device. They provide accurate and reliable measurements of temperature and humidity, making them popular choices for environmental monitoring applications.



### Pinout and Connections:

Look for the number 7 connections on the Sensor Shield as marked on the following picture.

| Sensor:      | Sensor Shield: |
| ------------ | -------------- |
| VCC (+)      | V              |
| DATA (S) (D) | S              |
| GND (-)      | G              |



<figure><img src="../.gitbook/assets/Arduino-DHT11-3.jpg" alt=""><figcaption></figcaption></figure>

<figure><img src="../.gitbook/assets/2017-04-15_3984-1536x1536 (1).jpg" alt=""><figcaption></figcaption></figure>

* Connect the positive (+) pin of the DHT sensor to the 5V pin on the Arduino.
* Connect the negative (-) pin of the DHT sensor to the GND pin on the Arduino.
* Connect the signal pin of the DHT sensor to digital pin D7 on the Arduino.



### Install the DHT library in the Arduino IDE:

<figure><img src="../.gitbook/assets/image (2).png" alt=""><figcaption></figcaption></figure>

1. Open the Arduino IDE (Integrated Development Environment).
2. Click on "Sketch" in the menu bar and navigate to "Include Library" > "Manage Libraries...".
3. The Library Manager window will open. In the search bar, type "DHT" and press Enter.
4. Look for the "DHT sensor library" by Adafruit. Once found, click on the "Install" button to install the library.
5. After the installation is complete, you can close the Library Manager window.
6. To use the library in your sketch, you need to include it at the beginning of your code. Add the following line at the top of your sketch:



### Code:

Open a new file on the Arduino IDE, copy this code and paste it in the new file.

```cpp
#include <DHT.h>

// DHT Sensor Pin
int dhtPin = 7;

// Initialize DHT sensor object
DHT dht(dhtPin, DHT11);

void setup() {
  // Initialize serial communication
  Serial.begin(9600);
  // Initialize DHT sensor
  dht.begin();
}

void loop() {
  // Read temperature in Celsius
  float temperature = dht.readTemperature();

  // Read humidity
  float humidity = dht.readHumidity();

  // Print temperature and humidity to serial monitor
  Serial.print("Temperature: ");
  Serial.print(temperature);
  Serial.print(" °C, Humidity: ");
  Serial.print(humidity);
  Serial.println(" %");

  delay(2000);
}
```

### Code Explanation:

* The code starts by including the `DHT.h` library, which provides functions to interact with the DHT sensor.
* The DHT sensor pin and object are initialized. In this example, we are using DHT11, so we create a `DHT` object with the specified pin and sensor type.
* In the `setup()` function, serial communication is initialized using `Serial.begin()`, and the DHT sensor is started with `dht.begin()`.
* Within the `loop()` function, temperature and humidity values are read from the DHT sensor using `dht.readTemperature()` and `dht.readHumidity()` respectively.
* The temperature and humidity values are printed to the serial monitor using `Serial.print()` and `Serial.println()`.
* A delay of 2 seconds (2000 milliseconds) is added before the next reading.

### Explanation of Technology:

The DHT sensors, such as DHT11 and DHT22, utilize a digital communication protocol to provide accurate measurements of temperature and humidity. The DHT library simplifies the interaction with these sensors by providing functions to read temperature and humidity values.

In this project, we are using the DHT11 sensor, which is capable of measuring temperature with a resolution of 1°C and humidity with a resolution of 1%. The sensor communicates with the Arduino using a single wire protocol.

By reading temperature and humidity values from the DHT sensor, we can gather important environmental data that is crucial for monitoring and controlling the smart garden system.

