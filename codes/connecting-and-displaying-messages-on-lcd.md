# Connecting and Displaying Messages on LCD

<figure><img src="../.gitbook/assets/R (1).jpg" alt=""><figcaption></figcaption></figure>

## LCD SCREEN (16x2):

LCD (Liquid Crystal Display) is a flat-panel display technology that uses liquid crystals to produce visual output. It is commonly used to display text and simple graphics in various electronic devices. In this project, we will be using an LCD display to provide visual feedback and information about the system status.



### Pinout and Connections:

Look for the IIC connections on the Sensor Shield as marked on the following picture.

| Sensor: | Sensor Shield: |
| ------- | -------------- |
| GND     | -              |
| VCC     | +              |
| SDA     | SDA            |
| SCL     | SCL            |

<figure><img src="../.gitbook/assets/lcd-i2c-pinout.webp" alt=""><figcaption></figcaption></figure>

<figure><img src="../.gitbook/assets/Sensor Shield I2C.jpg" alt=""><figcaption></figcaption></figure>

* Connect the SDA pin of the LCD to the SDA (A4) pin on the Arduino.
* Connect the SCL pin of the LCD to the SCL (A5) pin on the Arduino.
* Connect the VCC and GND pins of the LCD to the 5V and GND pins on the Arduino, respectively.



### Installing libraries for the LCD display:

1. Open the Arduino IDE (Integrated Development Environment).
2. Click on "Sketch" in the menu bar and navigate to "Include Library" > "Manage Libraries...".
3. The Library Manager window will open. In the search bar, type "LiquidCrystal\_I2C" and press Enter.
4. Look for the "LiquidCrystal I2C" library by Frank de Brabander. Once found, click on the "Install" button to install the library.
5. After the installation is complete, you can close the Library Manager window.
6. To use the library in your sketch, you need to include it at the beginning of your code. Add the following line at the top of your sketch

With these steps, you have successfully loaded and installed the LiquidCrystal\_I2C library in your Arduino IDE. You can now use the library to control the LCD display in your project.



{% hint style="info" %}
Some LCD screens have different I2C addresses, so If after running the next code your screen does not show any text, change the address on the code to:&#x20;

<pre class="language-arduino"><code class="lang-arduino"><strong>int lcdAddress = 0x3F;
</strong></code></pre>

You can use it by uncommenting this line in the code (erasing the "//" before the code and commenting on the 0x27 one (adding "//" at the beginning.
{% endhint %}



### Code:

Open a new file on the Arduino IDE, copy this code and paste it into the new file.

```arduino
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

// LCD module address
int lcdAddress = 39; //Common LCD address
// int lcdAddress = 0x3F; //Uncomment this line and run the code again if the screen doesn't show any text.

// LCD module dimensions
int lcdColumns = 16;
int lcdRows = 2;

// Create LCD object
LiquidCrystal_I2C lcd(lcdAddress, lcdColumns, lcdRows);

// Backlight brightness (0-255)
int backlightBrightness = 100;  // Adjust the value as needed

void setup() {
  // Initialize LCD
  lcd.begin(lcdColumns, lcdRows);

  // Set backlight brightness
  lcd.setBacklight(backlightBrightness);

  // Print welcome messages
  lcd.setCursor(0, 0);
  lcd.print("   Welcome to");
  lcd.setCursor(0, 1);
  lcd.print("     POWAR");
  delay(2000);
  
 // Food Shidt 3.0 message 
  lcd.setCursor(0, 0);
  lcd.print("FoodShift 3.0");
  lcd.setCursor(0, 1);
  lcd.print("Smart Garden");
  delay(2000);
}

void loop() {
  // Code for other functionalities
  // ...
}
```

### Code Explanation:

* The code starts by including the necessary libraries: `Wire.h` for I2C communication and `LiquidCrystal_I2C.h` for controlling the LCD.
* The LCD module's address, number of columns, and number of rows are declared.
* An LCD object, `lcd`, is created using the declared address, columns, and rows.
* In the `setup()` function, the LCD is initialized using `lcd.begin()`.
* The welcome messages are printed on the LCD using `lcd.setCursor()` to set the cursor position and `lcd.print()` to display the text.



### Explanation of Technology:

In this project, we are using an I2C-based LCD display. I2C (Inter-Integrated Circuit) is a communication protocol that allows multiple devices to communicate with each other using a two-wire interface. The I2C protocol simplifies the wiring and reduces the number of pins required to connect the LCD to the Arduino.

The LiquidCrystal\_I2C library provides functions to control the LCD using the I2C protocol. It allows us to initialize the LCD, set the cursor position, and print text on the display. By utilizing the LCD, we can provide visual feedback and display important information about the system, enhancing the user experience.
