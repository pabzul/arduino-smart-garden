# Smart Garden Automation with Arduino

&#x20;In this tutorial, we will create a smart garden automation system using an Arduino board. The system will monitor the soil moisture level, temperature, and humidity, and control the watering of plants using a water pump. We will display the sensor readings on an LCD screen for real-time monitoring. Let's get started!

### **Components:**

* Arduino Uno board
* Soil moisture sensor
* DHT11 temperature and humidity sensor
* Relay module
* I2C LCD display (16x2)

### **Connections:**

1. Connect the soil moisture sensor:
   * Connect the VCC pin to +5V on the Arduino.
   * Connect the GND pin to GND on the Arduino.
   * Connect the A0 pin to A0 on the Arduino.
2. Connect the relay module:
   * Connect the VCC pin to +5V on the Arduino.
   * Connect the GND pin to GND on the Arduino.
   * Connect the IN pin to digital pin 11 on the Arduino.
3. Connect the I2C LCD display:
   * Connect the SDA pin to the SDA (A4) pin on the Arduino.
   * Connect the SCL pin to the SCL (A5) pin on the Arduino.
   * Connect the VCC pin to +5V on the Arduino.
   * Connect the GND pin to GND on the Arduino.
4. Connect the DHT11 sensor:
   * Connect the VCC pin to +5V on the Arduino.
   * Connect the GND pin to GND on the Arduino.
   * Connect the OUT pin to digital pin 7 on the Arduino.

### **Library Installation:**

To use the DHT11 sensor and the LCD display, we need to install the following libraries:

1. DHT Library by Adafruit: To install this library, open the Arduino IDE, go to "Sketch" -> "Include Library" -> "Manage Libraries". Search for "DHT" and click the "Install" button.
2. LiquidCrystal\_I2C Library: To install this library, go to "Sketch" -> "Include Library" -> "Manage Libraries". Search for "LiquidCrystal\_I2C" and click the "Install" button.

### **Code:**

```cpp
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <DHT.h>

// Soil Moisture Sensor Pin
int soilMoisturePin = A0;

// Calibration Variables
int dry = 1023;    // Replace with your dry value
int wet = 0;       // Replace with your wet value

// Plant Moisture Threshold
int plantMoistureThreshold = 50;  // Moisture level at which the plant needs watering

// Water Pump Pin
int pumpPin = 11;

// DHT Sensor Pin
int dhtPin = 7;

// LCD module address
int lcdAddress = 0x27;

// LCD module dimensions
int lcdColumns = 16;
int lcdRows = 2;

// Backlight brightness (0-255)
int backlightBrightness = 100;  // Adjust the value as needed

// Initialize LCD, DHT, and Relay objects
LiquidCrystal_I2C lcd(lcdAddress, lcdColumns, lcdRows);
DHT dht(dhtPin, DHT11);

void setup() {
  // Initialize serial communication
  Serial.begin(9600);

  // Initialize LCD
  lcd.begin(lcdColumns, lcdRows);

  // Set backlight brightness
  lcd.setBacklight(backlightBrightness);

  // Print welcome messages
  lcd.setCursor(0, 0);
  lcd.print("   Welcome to");
  lcd.setCursor(0, 1);
  lcd.print("     POWAR");
  delay(2000);
  
 // Food Shidt 3.0 message 
  lcd.setCursor(0, 0);
  lcd.print("FoodShift 3.0");
  lcd.setCursor(0, 1);
  lcd.print("Smart Garden");
  delay(2000);

  // Initialize DHT sensor
  dht.begin();

  // Set relay pin as output
  pinMode (relayPin, OUTPUT);
}

void loop() {
  // Read soil moisture value
  int soilMoisture = analogRead(soilMoisturePin);

  // Map soil moisture value to a range of 0-100
  int moisturePercentage = map(soilMoisture, dry, wet, 0, 100);

  // Read temperature and humidity
  float temperature = dht.readTemperature();
  float humidity = dht.readHumidity();

  // Print sensor readings
  Serial.print("Soil Moisture: ");
  Serial.print(moisturePercentage);
  Serial.print("%, Temperature: ");
  Serial.print(temperature);
  Serial.print(" °C, Humidity: ");
  Serial.print(humidity);
  Serial.println(" %");

  // Control relay based on soil moisture threshold
  if (moisturePercentage < plantMoistureThreshold) {
    digitalWrite(relayPin, HIGH);  // Turn on the water pump
    lcd.clear();
    lcd.print("Watering Plant");
  } else {
    digitalWrite(relayPin, LOW);  // Turn off the water pump
    lcd.clear();
    lcd.print("Plant Happy");
  }

  // Display moisture percentage on LCD
  lcd.setCursor(0, 1);
  lcd.print("Moisture: ");
  lcd.print(moisturePercentage);
  lcd.print("%");

  // Display temperature and humidity on LCD
  lcd.setCursor(0, 2);
  lcd.print("Temp: ");
  lcd.print(temperature);
  lcd.print("C, Humidity: ");
  lcd.print(humidity);
  lcd.print("%");

  delay(2000);
}
```

### **Code Explanation:**

* We start by including the necessary libraries: Wire for I2C communication, LiquidCrystal\_I2C for the LCD display, and DHT for the DHT11 sensor.
* The pin connections for the soil moisture sensor, relay module, LCD display, and DHT11 sensor are defined.
* The LCD and DHT objects are initialized with their respective pins and parameters.
* In the `setup()` function, we set up the serial communication, initialize the LCD, and configure the relay pin as an output.
* The `loop()` function reads the soil moisture level using the analogRead() function and maps it to a percentage value using the `moistureMin` and `moistureMax` variables.
* The temperature and humidity are read from the DHT11 sensor using the `readTemperature()` and `readHumidity()` functions.
* The sensor readings are printed to the serial monitor for debugging purposes.
* Based on the moisture threshold, the relay is controlled to turn the water pump on or off. The LCD displays the corresponding message.
* The moisture percentage, temperature, and humidity values are displayed on the LCD.

### **Conclusion:**

In this tutorial, we have learned how to create a smart garden automation system using Arduino. We have connected and configured various sensors and modules to monitor soil moisture, temperature, and humidity. The system controls the watering of plants based on the moisture level and displays sensor readings on an LCD screen. With this project, you can efficiently automate and monitor your garden, ensuring optimal plant health.

Feel free to experiment with different sensor thresholds and customize the system according to your specific requirements.



> This final code has been inspired on the Techatronic tutorial.\
> [https://techatronic.com/how-to-make-smart-irrigation-project/](https://techatronic.com/how-to-make-smart-irrigation-project/)
