# Displaying DHT Sensor Values on LCD

<figure><img src="../.gitbook/assets/blob_yRVPcHHwwb.webp" alt=""><figcaption></figcaption></figure>

In this step, we will combine the previous functionalities of reading temperature and humidity from the DHT sensor and displaying messages on the LCD. The DHT sensor readings will be shown on the LCD screen, providing real-time information about the environmental conditions.

####

#### Pinout and Connections:

* Ensure that you have followed the previous instructions to connect the LCD display and the DHT to the Arduino correctly.



### Code:

Open a new file on the Arduino IDE, copy this code and paste it in the new file.

```cpp
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <DHT.h>

// LCD module address
int lcdAddress = 0x27;

// LCD module dimensions
int lcdColumns = 16;
int lcdRows = 2;

// DHT Sensor Pin
int dhtPin = 7;

// Backlight brightness (0-255)
int backlightBrightness = 100;  // Adjust the value as needed

// Initialize LCD and DHT sensor objects
LiquidCrystal_I2C lcd(lcdAddress, lcdColumns, lcdRows);
DHT dht(dhtPin, DHT11);

void setup() {
  // Initialize serial communication
  Serial.begin(9600);
  
  // Initialize LCD
  lcd.begin(lcdColumns, lcdRows);

  // Set backlight brightness
  lcd.setBacklight(backlightBrightness);

  // Print welcome messages
  lcd.setCursor(0, 0);
  lcd.print("   Welcome to");
  lcd.setCursor(0, 1);
  lcd.print("     POWAR");
  delay(2000);
  
 // Food Shidt 3.0 message 
  lcd.setCursor(0, 0);
  lcd.print("FoodShift 3.0");
  lcd.setCursor(0, 1);
  lcd.print("Smart Garden");
  delay(2000);

  // Initialize DHT sensor
  dht.begin();
}

void loop() {
  // Read temperature and humidity from DHT sensor
  float temperature = dht.readTemperature();
  float humidity = dht.readHumidity();


  // Print temperature and humidity to serial monitor
  Serial.print("Temperature: ");
  Serial.print(temperature);
  Serial.print(" °C, Humidity: ");
  Serial.print(humidity);
  Serial.println(" %");

  delay(2000);

  // Clear LCD display
  lcd.clear();

  // Display temperature on LCD
  lcd.setCursor(0, 0);
  lcd.print("Temp C:   ");
  lcd.print(temperature);
  lcd.print("C");

  // Display humidity on LCD
  lcd.setCursor(0, 1);
  lcd.print("Humidity: ");
  lcd.print(humidity);
  lcd.print("%");

  delay(2000);
}
```



### Code Explanation:

* The code starts by including the necessary libraries: `Wire.h` for I2C communication, `LiquidCrystal_I2C.h` for controlling the LCD, and `DHT.h` for interacting with the DHT sensor.
* The LCD module's address, number of columns, and number of rows are declared.
* An LCD object, `lcd`, and a DHT object, `dht`, are created using the respective addresses and pins.
* In the `setup()` function, the LCD is initialized using `lcd.begin()`, and the welcome messages are printed on the LCD.
* The DHT sensor is started with `dht.begin()`.
* Within the `loop()` function, temperature and humidity values are read from the DHT sensor using `dht.readTemperature()` and `dht.readHumidity()` respectively.
* The LCD display is cleared using `lcd.clear()` to remove any previous content.
* The temperature and humidity values are displayed on the LCD using `lcd.setCursor()` to set the cursor position and `lcd.print()` to display the text and values.
* A delay of 2 seconds (2000 milliseconds) is added before the next reading.



### Explanation of Technology:&#x20;

By combining the functionality of reading temperature and humidity values from the DHT sensor and displaying messages on the LCD, we can create a more comprehensive user interface. The LCD provides a visual representation of the environmental conditions, allowing users to easily monitor the temperature and humidity levels in real-time.

In the code, the LCD is initialized and the welcome messages are displayed at the beginning. Then, within the main loop, the temperature and humidity readings are obtained from the DHT sensor. The LCD display is cleared, and the temperature and humidity values are printed on the LCD screen.

This integration of the DHT sensor and LCD display enhances the user experience by providing an intuitive and informative interface for monitoring the smart garden system.
