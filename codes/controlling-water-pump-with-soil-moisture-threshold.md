# Controlling Water Pump with Soil Moisture Threshold

<figure><img src="../.gitbook/assets/JT40-Upright-DC-3V-0-36W-Mini-Submersible-Waterproof-Water-Pump-Fish-Tank-Circulate-Aquarium (1).jpg" alt=""><figcaption></figcaption></figure>

In this step, we will utilize the calibrated values from the soil moisture sensor to control a water pump. The pump will be activated when the soil moisture level falls below a certain threshold, indicating that the plant needs watering.

<figure><img src="../.gitbook/assets/OIP (1).jpg" alt=""><figcaption></figcaption></figure>

### **Relay:**

A relay is an electrical switch that is operated by an electromagnet. It is commonly used in circuits to control high-power devices using a low-power signal. The relay consists of a coil, an armature, and one or more sets of contacts.

When a current flows through the coil, it generates a magnetic field that attracts the armature, causing it to move and close or open the contacts. This action allows the relay to control the flow of current in a separate circuit that may have a different voltage or current rating.

In this project, a relay is used to control the water pump. The signal pin of the relay module is connected to a digital pin on the Arduino. When the digital pin is set to HIGH, it energizes the relay coil, closing the contacts and allowing current to flow through the water pump, turning it on. Conversely, when the digital pin is set to LOW, the relay contacts open, cutting off the current flow and turning the water pump off.

The use of a relay in this project allows the Arduino, which operates at low voltages and currents, to control a higher-power device like the water pump safely.

###

### Pinout and Connections:

* Connect the signal pin of the relay module to digital pin D11 on the Arduino.
* Connect the VCC and GND pins of the relay module to the 5V and GND pins on the Arduino, respectively.
* Connect the positive (+) terminal of the water pump (RED) to the normally close (NC) terminal of the relay.
* Connect the negative (-) terminal of the water pump to the GND pin on the Arduino.
* Connect a red wire (+) from the VCC pin on the Arduino to the common (COM) terminal of the relay.

###

### Code:

```arduino
// Soil Moisture Sensor Pin
int soilMoisturePin = A0;

// Calibration Variables
int dry = 1023;    // Replace with your dry value
int wet = 0;       // Replace with your wet value

// Water Pump Pin
int pumpPin = 11;

void setup() {
  // Initialize relay pin as OUTPUT
  pinMode(pumpPin, OUTPUT);
  // Initialize serial communication
  Serial.begin(9600);
}

void loop() {
  // Read soil moisture value
  int soilMoisture = analogRead(soilMoisturePin);

  // Map soil moisture value to a range of 0-100
  int moisturePercentage = map(soilMoisture, dry, wet, 0, 100);

  // Print moisture percentage to serial monitor
  Serial.print("Moisture Percentage: ");
  Serial.print(moisturePercentage);
  Serial.println("%");

  // Check if moisture level is below threshold
  if (moisturePercentage < 50) {
    // Turn on the water pump
    digitalWrite(pumpPin, HIGH);
    Serial.println("Water Pump ON");
  } else {
    // Turn off the water pump
    digitalWrite(pumpPin, LOW);
    Serial.println("Water Pump OFF");
  }

  delay(1000);
}
```

### Code Explanation:

* The code starts by declaring the soil moisture sensor pin, calibration variables (`dry`and `wet`), and the water pump pin.
* In the `setup()` function, the pump pin is set as an output pin using `pinMode()`, and serial communication is initialized.
* Within the `loop()` function, the soil moisture value is read using `analogRead()` from the soil moisture pin.
* The `map()` function is used to map the soil moisture value to a percentage range of 0-100 based on the calibrated minimum and maximum values.
* The moisture percentage is printed to the serial monitor.
* If the moisture percentage is below the threshold (70 in this case), the water pump is turned on by setting the pump pin to HIGH. Otherwise, the pump is turned off by setting the pump pin to LOW.
* A delay of 1 second is added before the next reading.
