# Arduino IDE

For this specific project, you will need to install the Arduino IDE software in order to code the WIO Terminal, the DHT library to read the DHT11 sensors, and the LCD screen library.&#x20;

To install the Arduino IDE click on the link below to open the web page with the installer.

{% embed url="https://www.arduino.cc/en/software" %}

* Select the version of Arduino IDE based on the operating system of your computer.

<figure><img src="https://files.gitbook.com/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F0EjH3yeoMoBQB9sdFgNw%2Fuploads%2FY7l7AEJijyLSJS8TyjHe%2Fimage.png?alt=media&#x26;token=809178ed-e3c0-49d1-9104-df6f332bcf77" alt=""><figcaption></figcaption></figure>

* Decide if you want to donate, or click on "Just Download"

<figure><img src="https://files.gitbook.com/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F0EjH3yeoMoBQB9sdFgNw%2Fuploads%2FxyBqCJwbn5vhmjxzMzop%2Fimage.png?alt=media&#x26;token=f31a2abf-51a0-4e20-ad9f-366e1f6b8dbd" alt=""><figcaption></figcaption></figure>

* After downloading the installer, execute it and follow the instructions.
