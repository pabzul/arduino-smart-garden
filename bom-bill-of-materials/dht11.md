# DHT11

<figure><img src="../.gitbook/assets/DHT11.jpg" alt=""><figcaption></figcaption></figure>

The DHT11 is a basic, low-cost temperature and humidity sensor module widely used in various Arduino projects and other electronic applications. It provides accurate measurements of ambient temperature and relative humidity, making it suitable for climate monitoring and control systems, weather stations, and indoor environmental monitoring.

The DHT11 sensor module contains a calibrated digital sensor with a built-in analog-to-digital converter and a communication interface. It consists of a humidity sensing component and a thermistor for temperature measurement. The sensor is capable of measuring humidity in the range of 20% to 90% with a ±5% accuracy and temperature in the range of 0°C to 50°C with a ±2°C accuracy.

The sensor operates on a single-wire communication protocol, making it easy to interface with microcontrollers like Arduino. It provides a digital output signal that can be read directly by the microcontroller. The communication with the DHT11 sensor involves sending a request signal and receiving a response containing the temperature and humidity data.

The DHT11 is a cost-effective solution for basic temperature and humidity monitoring needs. Its simplicity and affordability make it popular among hobbyists, students, and DIY enthusiasts. However, it's important to note that the DHT11 has limited accuracy and may not be suitable for applications that require high precision measurements. For more precise measurements, advanced sensors like the DHT22 or other specialized sensors may be preferred.
