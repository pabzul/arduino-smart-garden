---
cover: >-
  https://images.unsplash.com/photo-1603732551681-2e91159b9dc2?crop=entropy&cs=srgb&fm=jpg&ixid=M3wxOTcwMjR8MHwxfHNlYXJjaHw1fHxhcmR1aW5vfGVufDB8fHx8MTY5Njg2ODExNnww&ixlib=rb-4.0.3&q=85
coverY: 0
---

# BOM (Bill Of Materials)

<figure><img src="../.gitbook/assets/UNO_KIT.jpg" alt=""><figcaption><p>(This image is a reference, the kit also includes an LCD and a DHT11)</p></figcaption></figure>

Here are the materials you will need in order to build this project:

1. [**Arduino Board** ](arduino-board.md)**(e.g., Arduino Uno):** The main microcontroller board that serves as the brain of the project.
2. [**Soil Moisture Sensor:** ](soil-moisture-sensor.md)A sensor used to measure the moisture level in the soil. The sensor provides analog output based on the soil's moisture content.
3. [**DHT11 Temperature and Humidity Sensor:**](dht11.md) A sensor capable of measuring temperature and humidity in the environment.
4. [**Relay Module:** ](relay-module.md)A module that allows the Arduino to control high-power devices, such as a water pump. It acts as a switch to turn the pump on or off.
5. [**Water Pump:** ](water-pump.md)An actuator used to supply water to the plants when the moisture level falls below a certain threshold.
6. [**LCD Display (16x2):**](lcd-16x2-i2c.md) A liquid crystal display that provides a visual interface to display sensor readings and system information.
7. [**Arduino Sensor Shield:** ](sensor-shield-v5.md)A shield specifically designed for Arduino boards that provides easy and convenient connections for components. It simplifies the wiring process and ensures compatibility.
8. **Jumper Wires:** Wires used for connecting various components to the Arduino shield and making necessary connections.
9. **Power Supply:** A power source to supply power to the Arduino board and the water pump. It can be a USB cable connected to a computer or an external power adapter or battery pack.

Please note that the quantities and specific models may vary based on your preferences and availability. Ensure that the components you choose are compatible with the Arduino board and shield and have the required specifications.

It's recommended to check the datasheets and specifications of each component to ensure proper integration and functionality within the project.
