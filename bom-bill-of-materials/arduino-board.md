# Arduino Board



<figure><img src="../.gitbook/assets/ARDUINO UNO (2).jpg" alt=""><figcaption></figcaption></figure>

The Arduino board is a popular open-source microcontroller platform designed for building interactive electronic projects. It consists of a programmable circuit board with input/output pins that can be connected to various electronic components and sensors. The board is powered by an Atmel AVR microcontroller and can be programmed using the Arduino software development environment. Arduino boards are widely used by hobbyists, students, and professionals to create a wide range of projects, from simple LED blinking to complex robotic systems and home automation setups. The board's simplicity, versatility, and large community support make it an excellent choice for beginners and experienced users alike.



<figure><img src="../.gitbook/assets/Tutorial-de-Arduino-Uno-Pinout.png" alt=""><figcaption></figcaption></figure>

Here is a short description of the pinout configuration of the Arduino Uno:

1. Digital Pins (0-13): The Arduino Uno has a total of 14 digital input/output pins, labeled from 0 to 13. These pins can be used for both digital input (reading the state of a sensor or switch) and digital output (controlling LEDs, relays, or other devices).
2. Analog Pins (A0-A5): The Arduino Uno features 6 analog input pins, labeled A0 to A5. These pins can measure analog voltage levels from 0 to 5 volts, making them suitable for interfacing with analog sensors or reading variable inputs.
3. Power Pins: The Arduino Uno has several power pins:
   * 5V: This pin provides a regulated +5V output, which can be used to power external components or sensors.
   * 3.3V: This pin provides a regulated +3.3V output, suitable for low-power devices.
   * Vin: This pin allows you to supply power to the Arduino Uno using an external power source (7-12V).
   * GND: These pins are the ground connections and are used for completing electrical circuits.
4. Reset Button: The Arduino Uno includes a reset button that restarts the microcontroller and restarts the program execution from the beginning.
5. USB Connection: The Arduino Uno can be connected to a computer or other USB-enabled devices using a USB Type-B cable. This connection allows you to upload sketches (programs) to the board and communicate with the Arduino serial monitor for debugging purposes.

Understanding the pinout configuration of the Arduino Uno is essential for correctly connecting sensors, actuators, and other components to the board, enabling you to create a wide range of projects and explore the world of electronics and programming.
