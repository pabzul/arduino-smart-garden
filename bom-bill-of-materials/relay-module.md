# Relay Module:

<figure><img src="../.gitbook/assets/relay.jpg" alt=""><figcaption></figcaption></figure>

A relay is an electrical switch that is operated by an electromagnet to control the flow of current in an electrical circuit. It is commonly used in applications where a low-power control signal is used to control a higher-power load.

The relay consists of a coil and one or more sets of contacts. When current flows through the coil, it generates a magnetic field that activates the contacts, either opening or closing the circuit. This allows the relay to control the flow of electricity to a connected device or circuit.

Relays are used in a wide range of applications, including home automation, industrial control systems, automotive electronics, and power distribution. They are particularly useful for applications that require electrical isolation between the control circuit and the load circuit, as relays can provide this isolation.

One of the key advantages of relays is their ability to control high-current or high-voltage circuits using a lower-power control signal. This makes them versatile in switching various types of loads, such as motors, lights, solenoids, and heaters. Additionally, relays offer durability, long lifespan, and reliable operation, making them suitable for both small-scale and large-scale electrical systems.

Relays can be controlled by a variety of input signals, including digital signals from microcontrollers, switches, sensors, or even other relays. They come in different configurations, such as single-pole single-throw (SPST), single-pole double-throw (SPDT), and multiple-pole relays, to accommodate various circuit requirements.

